﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WikeSoft.WorkFlowEngine
{
    class NodeType
    {
        public const string Start = "start";

        public const string Task = "task";

        public const string End = "end";
    }
}
