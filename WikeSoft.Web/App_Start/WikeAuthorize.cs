﻿using System.Web;
using System.Web.Mvc;

namespace WikeSoft.Web
{
    public class WikeAuthorizeAttribute : AuthorizeAttribute
    {

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return base.AuthorizeCore(httpContext);
            //return DateTime.Now.Minute%2 == 0;
        }


        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            bool isajax = filterContext.RequestContext.HttpContext.Request.IsAjaxRequest();
            if (isajax)
            {
                filterContext.Result = new JavaScriptResult() {Script = "alert(\"请重新登录\");window.location.href = \"/home/login\";" };
            }
            else
            {
                filterContext.Result = new RedirectResult("/Home/Login");
            }
           

            //base.HandleUnauthorizedRequest(filterContext);
        }
       
    }
}