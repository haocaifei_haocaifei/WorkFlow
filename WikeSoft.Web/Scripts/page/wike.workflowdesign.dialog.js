﻿function openPostDialog() {
    top.layer.open({
        title: '功能查询',
        type: 2,
        content: "/WorkFlowCategory/ListView",
        area: ['1000px', '550px'],
        btn: ['确认', '关闭'],
        btnclass: ['btn btn-primary', 'btn btn-danger'],
        yes: function (index, layero) {
            var tem = $(layero).find("iframe")[0].contentWindow.getContent();
            setParentData(tem.Id, tem.CategoryName);
            top.layer.close(index);
        }, cancel: function () {
            return true;
        }
    });
}
function setParentData(id, name) {
    $("#CategoryId").val(id);
    $("#CategoryName").val(name);
}


$(document).ready(function () {
    $("#btnSelectPost").click(openPostDialog);

    $("#btnClear").click(function () {
        $("#CategoryId").val("");
        $("#CategoryName").val("");
    });
});