﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.Data.Enum
{
    public enum CompanyCertificateStatusEnum
    {
        /// <summary>
        /// 资源客户
        /// </summary>
        [Description("资源客户")]
        Resource = 0,

        /// <summary>
        /// 意向客户
        /// </summary>
        [Description("意向客户")]
        Interested = 1,

        /// <summary>
        /// 已签客户
        /// </summary>
        [Description("已签客户")]
        Signed = 2,

        /// <summary>
        /// 库存客户，已签客户转为合同完成时 
        /// </summary>
        [Description("库存客户")]
        Store = 3,
 

        /// <summary>
        /// 信誉不良
        /// </summary>
        [Description("信誉不良")]
        BadReputation =5,

        /// <summary>
        /// 合同完成
        /// </summary>
        [Description("合同完成")]
        FinishContract =6,
        /// <summary>
        /// 问题证书
        /// </summary>
        [Description("问题证书")]
        HaveQuestion = 7,
        /// <summary>
        /// 解除合作
        /// </summary>
        [Description("解除合作")]
        RelieveCooperation = 8,

        /// <summary>
        /// 入库申请
        /// </summary>
        [Description("入库申请")]
        StoreApply = 10,
    }
}
