﻿using System.ComponentModel;

namespace WikeSoft.Data.Enum
{
    /// <summary>
    /// 客户类别字典
    /// </summary>
    public enum CustomerTypeEnum
    {
        /// <summary>
        /// 资源客户
        /// </summary>
        [Description("资源客户")]
        ResourceCustomer = 0,

        /// <summary>
        /// 意向客户
        /// </summary>
        [Description("意向客户")]
        InterestedCustomer = 1,

        /// <summary>
        /// 已签客户
        /// </summary>
        [Description("已签客户")]
        SignedCustomer =2,

        /// <summary>
        /// 库存客户
        /// </summary>
        [Description("库存客户")]
        StoreCustomer = 3
         
    }
}
