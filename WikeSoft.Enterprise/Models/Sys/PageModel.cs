﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WikeSoft.Enterprise.Models.Sys
{
    

    /// <summary>
    /// 页面添加模型
    /// </summary>
    public class PageModel
    {

        public string Id { get; set; }
        ///<summary>
        /// 父节点
        ///</summary>
        public string ParentId { get; set; }

        ///<summary>
        /// 页面编码（主要进行排序）
        ///</summary>
        public string PageCode { get; set; } // PageCode (length: 500)

        ///<summary>
        /// 页面名称
        ///</summary>
        public string PageName { get; set; } // PageName (length: 500)

        ///<summary>
        /// 页面路径
        ///</summary>
        public string PagePath { get; set; } // PagePath (length: 500)

        ///<summary>
        /// 说明（绑定到title)
        ///</summary>
        public string Description { get; set; } // Description (length: 500)

        ///<summary>
        /// 是否可用
        ///</summary>
        public bool? IsUsed { get; set; } // IsUsed

        ///<summary>
        /// 样式名称（主要针对特殊的样式）
        ///</summary>
        public string CssName { get; set; } // CssName (length: 500)
    }

    /// <summary>
    /// 页面添加模型
    /// </summary>
    public class PageAddModel
    {
        ///<summary>
        /// 父节点
        ///</summary>
        public string ParentId { get; set; }

        /// <summary>
        /// 父页面名称
        /// </summary>
        [Display(Name = "父页面名称")]
        public string ParentPageName { get; set; }

        ///<summary>
        /// 页面编码（主要进行排序）
        ///</summary>
        [Display(Name = "页面编码")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        [MaxLength(500, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string PageCode { get; set; } // PageCode (length: 500)

        ///<summary>
        /// 页面名称
        ///</summary>
        [Display(Name = "页面名称")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        [MaxLength(500, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string PageName { get; set; } // PageName (length: 500)

        ///<summary>
        /// 页面路径
        ///</summary>
        [Display(Name = "页面路径")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        [MaxLength(500, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string PagePath { get; set; } // PagePath (length: 500)

        ///<summary>
        /// 说明（绑定到title)
        ///</summary>
        [Display(Name = "说明")]
        [MaxLength(500, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string Description { get; set; } // Description (length: 500)

        ///<summary>
        /// 样式名称（主要针对特殊的样式）
        ///</summary>
        [Display(Name = "样式名称")]
        [MaxLength(500, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string CssName { get; set; }

        /// <summary>
        /// 是否启用
        /// </summary>
        [Display(Name = "是否启用")]
        public bool IsUsed { get; set; }

        /// <summary>
        /// 角色ID集合
        /// </summary>
        public List<string> RoleIds { get; set; }
    }

    /// <summary>
    /// 页面编辑模型
    /// </summary>
    public class PageEditModel
    {
        

        public string Id { get; set; }

        ///<summary>
        /// 父节点
        ///</summary>
        public string ParentId { get; set; }

        /// <summary>
        /// 父页面名称
        /// </summary>
        [Display(Name = "父页面名称")]
        public string ParentPageName { get; set; }

        ///<summary>
        /// 页面编码（主要进行排序）
        ///</summary>
        [Display(Name = "页面编码")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        [MaxLength(500, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string PageCode { get; set; } // PageCode (length: 500)

        ///<summary>
        /// 页面名称
        ///</summary>
        [Display(Name = "页面名称")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        [MaxLength(500, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string PageName { get; set; } // PageName (length: 500)

        ///<summary>
        /// 页面路径
        ///</summary>
        [Display(Name = "页面路径")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        [MaxLength(500, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string PagePath { get; set; } // PagePath (length: 500)

        ///<summary>
        /// 说明（绑定到title)
        ///</summary>
        [Display(Name = "页面说明")]
        [MaxLength(500, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string Description { get; set; } // Description (length: 500)

        ///<summary>
        /// 样式名称（主要针对特殊的样式）
        ///</summary>
        [Display(Name = "样式名称")]
        [MaxLength(500, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string CssName { get; set; }

        /// <summary>
        /// 是否启用
        /// </summary>
        [Display(Name = "是否启用")]
        public bool IsUsed { get; set; }

        /// <summary>
        /// 角色ID集合
        /// </summary>
        public List<string> RoleIds { get; set; }
    }
}
